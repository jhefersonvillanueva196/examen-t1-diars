﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Models
{
    public class Comentario
    {
        public int id_Comentario { get; set; }
        public int id_Publicacion { get; set; }
        public string contenido { get; set; }
        public DateTime fecha_C { get; set; }
    }
}

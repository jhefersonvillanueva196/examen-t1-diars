﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Models
{
    public class Post
    {
        public int id_Publicacion { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public string contenido_P { get; set; }
        public DateTime fecha_P { get; set; }
    }
}

﻿using Examen_T1.Datos.Configuraciones;
using Examen_T1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Datos
{
    public class AppPublicaciones: DbContext
    {
        public DbSet<Post> publicacion { get; set; }
        public DbSet<Comentario> comentarios { get; set; }  

        public AppPublicaciones(DbContextOptions<AppPublicaciones> options): base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PuMap());
            modelBuilder.ApplyConfiguration(new ComMap());
        }
    }
}

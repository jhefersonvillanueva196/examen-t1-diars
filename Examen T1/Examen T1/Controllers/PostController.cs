﻿using Examen_T1.Datos;
using Examen_T1.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Controllers
{
    public class PostDetalle
    {
        public Post Posts { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }

    public class PostController : Controller
    {
        private AppPublicaciones context;

        public PostController(AppPublicaciones context)
        {
            this.context = context;
        }
        [HttpGet]
        
        public ViewResult Detalle(int id)
        {
            var posts = context.publicacion;

            Post pu = posts.FirstOrDefault(item => item.id_Publicacion == id);
            
            


            List<Comentario> comentarios = context.comentarios.Where(o => o.id_Publicacion == id).ToList(); 

            var detalle = new PostDetalle
            {
              Posts = pu,
              Comentarios = comentarios
            };

            return View(detalle);


        }
        public RedirectToActionResult AddComentario(Comentario comentario)
        {
            context.comentarios.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("Detalle", new { id = comentario.id_Publicacion });
        }
    }
    

}

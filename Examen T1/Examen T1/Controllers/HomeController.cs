﻿using Examen_T1.Datos;
using Examen_T1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Controllers
{
    public class HomeController : Controller
    {
        private AppPublicaciones context;

        public HomeController(AppPublicaciones context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var post = context.publicacion;
            return View("Index", post);
        }
    }
}

create database T1_DIARS

use T1_DIARS

create table Publicacion(
	id_Publicacion int primary key not null identity(1,1),
	titulo varchar(50),
	autor varchar(30),
	contenido_P varchar(200),
	fecha_P datetime
)
create table Comentario(
	id_Comentario int primary key not null identity(1,1),
	id_Publicacion int,
	Contenido_C varchar(200),
	fecha_C datetime,
	constraint FK_Pu_Com foreign key (id_Publicacion) references Publicacion(id_Publicacion)
)

